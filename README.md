# Membres du groupe
Ratrimoarison Cassandra
Laroche Rémy
# Globe-Trends

Projet de visualisation 3D utilisant ThreeJS.

Le but est d'afficher un globe en 3D avec les interêts des recherches par pays. <br>
Cet outil se base sur Google trend (https://trends.google.com/trends/?geo=US) <br>

![Screenshot](https://gitlab.com/RMLaroche/globe-trends/-/raw/master/images/Capture.PNG?raw=true")

# Quickstart

Pour visualiser le résultat de ce projet, simplement lancer l'image docker :
```
docker run -d --name globe-trends -p 8080:80 rmlaroche/globe-trends:latest

```

### Fonctionnement des pourcentages

Les résultats vont de 0 à 100, où 100 correspond à la région ayant enregistré le plus fort pourcentage d'utilisation de ce mot clé par rapport au nombre total de recherches locales. Une valeur de 50 signifie que le mot clé a été utilisé moitié moins souvent dans la région concernée, et une valeur de 0 correspond à une région n'ayant pas enregistré assez de données pour ce mot clé
 
## Contacts
remy.laroche@epsi.fr <br>
n.ratrimoarison@epsi.fr



