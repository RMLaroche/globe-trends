//Met à jour les données en fonction de la recherche en cours
function update_data(name) {
    file_csv = "csv/" + name + ".csv";
    d3.csv(file_csv, function (csvData) {
        for (var i = 0; i < countries.length; i++) {
            if (csvData.Country == countries[i].name) {
                countries[i].porcentage = parseInt(csvData.porcentage, 10) || 0;
            }
        }

    }).then(function () {
        countries.sort(compare);

        markers = new Markers(countries);
        //get_country_order();
        order_data = countries.sort(compare);
        get_country_order(order_data);
    });
}

function compare(a, b) {
    const bandA = a.porcentage;
    const bandB = b.porcentage;

    let comparison = 0;
    if (bandA > bandB) {
        comparison = -1;
    } else if (bandA < bandB) {
        comparison = 1;
    }
    return comparison;

}

//Créé la liste de pays par ordre décroissant (overlay à droite)
function get_country_order(order_data) {
    data_countries = order_data;
    const getelementul = document.getElementById('country_ul');
    for (var i = 0; i < 20; i++) {
        if (data_countries[i].porcentage > 0) {
            const li = document.createElement("li")
            li.innerHTML = "<h3>" + data_countries[i].name + " (" + data_countries[i].porcentage + " %) </h3>";
            li.setAttribute('id', "country_" + [i]);
            li.setAttribute('class', "country_off");

            getelementul.appendChild(li);
        }
    }

}

