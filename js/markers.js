//La classe Markers correspond à l'ensemble des markers
class Markers {
    constructor(countries) {
        this.countries = countries;
        this.radius_marker = 1;
        this.radius = globe_radius * this.radius_marker;

        this.marker_spere = new THREE.SphereGeometry(this.radius_marker, 15, 15);

        this.removePreviousMarkers()
        this.create();
    }
    //Enlève les markers précédents
    removePreviousMarkers() {
        for (var i = data_group.children.length - 1; i >= 0; i--) {
            data_group.remove(data_group.children[i]);
        }
    }
    //Créé les nouveaux markers
    create() {

        for (let i = 0; i < this.countries.length; i++) {
            const country = this.countries[i];
            if (country.latitude && country.longitude) {
                const lat = +country.latitude;
                const lng = +country.longitude;

                var radiusValue
                var colour
                if (this.countries[i].porcentage != null) {

                    radiusValue = globe_radius * ((this.countries[i].porcentage * 0.5) / 100 + 1);
                    const gb_colours = (Math.trunc(255 - (this.countries[i].porcentage * 2.55))).toString()

                    colour = "rgb(255, " + gb_colours + ", " + gb_colours + ")"
                } else {
                    colour = "rgb(255, 255, 255)"
                    radiusValue = this.radius
                }
                const material = new THREE.MeshBasicMaterial({color: colour});
                const cords = sphere_coordinates(lat, lng, this.radius);
                const cordshigh = sphere_coordinates(lat, lng, radiusValue);
                new Marker(material, this.marker_spere, country.name, cords, cordshigh);

            }
        }

    }
}
