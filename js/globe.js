var renderer = new THREE.WebGLRenderer();
document.body.appendChild(renderer.domElement);
renderer.setSize(window.innerWidth, window.innerHeight);

//Creation de la camera
var camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 0.1, 1000);

//creation de la scene
var scene = new THREE.Scene();

//couleur de fond
scene.background = new THREE.Color(0, 0, 0);

function updateViewportSize() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix()
    renderer.setSize(window.innerWidth, window.innerHeight);
}

window.addEventListener("resize", updateViewportSize);
updateViewportSize();

//Rayon du globe
globe_radius = 200;


//ajout de la texture
texture_terre = new THREE.TextureLoader().load("texture/earth.jpg");

//création du premier cube et ajout dans le groupe 1, 32,32,6,6.3,0,6.3 {color:0xFFFFFF}
var terre = new THREE.SphereGeometry(globe_radius, 64, 64);
var terre_material = new THREE.MeshStandardMaterial({map: texture_terre});
terreMesh = new THREE.Mesh(terre, terre_material);
terreMesh.position.x = 0;
group.add(terreMesh);


//Orbit controls
var controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.minDistance = 250;
controls.maxDistance = 1000;
controls.enablePan = false;

// positionnement de la caméra
camera.position.set(0, 350, 400);

//Ambient Light
var ambientLight = new THREE.AmbientLight(0xffffff, .2);
scene.add(ambientLight);

// Directional light
var directionalLight = new THREE.DirectionalLight(0xffdddd, .6);
directionalLight.position.set(1, 1, 1);
scene.add(directionalLight);

// Point light
var pointLight = new THREE.DirectionalLight(0xaaafff, .35);
pointLight.position.set(-2, 1, 1);
scene.add(pointLight);

//Ajout des markers au groupe principal
group.add(data_group);

//Ajout du groupe à la scene
scene.add(group);

function animate() {
    controls.update();
    requestAnimationFrame(animate);
    renderer.render(scene, camera);
}

animate();