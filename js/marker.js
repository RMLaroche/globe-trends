//La classe marker correspond à l'affichage de données d'un pays.
class Marker {
    constructor(material, geometry, label, cords, radiusValue) {
        this.material = material;
        this.geometry = geometry;
        this.labelText = label;
        this.cords = cords;
        this.cordsmax = radiusValue;

        this.isAnimating = false;

        this.textColor = 'white';
        this.group_marker = new THREE.Group();

        //Créé le label (text avec le nom de pays)
        this.createLabel();
        //Créé les points et la ligne du pays
        this.createPoint();
        //Ajout au groupe data_group
        data_group.add(this.group_marker);
    }

    //Créé le label (text avec le nom de pays)
    createLabel() {
        const text = this.createText();
        const texture = new THREE.CanvasTexture(text);
        texture.minFilter = THREE.LinearFilter;


        const material = new THREE.SpriteMaterial();
        material.map = texture;

        this.label = new THREE.Sprite(material);
        this.label.scale.set(40, 20, 1);
        var {x, y, z} = this.cordsmax;
        this.label.position.set(-x, y, -z);
        this.label.translateY(2);
        this.group_marker.add(this.label);
    }

    //Créé les points et la ligne du pays
    createPoint() {
        const points = [];

        this.point = new THREE.Mesh(this.geometry, this.material);
        var {x, y, z} = this.cords;
        points.push(new THREE.Vector3(-x, y, -z));
        this.point.position.set(-x, y, -z);
        this.group_marker.add(this.point);


        this.point2 = new THREE.Mesh(this.geometry, this.material);
        var {x, y, z} = this.cordsmax;
        points.push(new THREE.Vector3(-x, y, -z));
        this.point2.position.set(-x, y, -z);
        this.group_marker.add(this.point2);


        const geometry = new THREE.BufferGeometry().setFromPoints(points);
        const material = new THREE.LineBasicMaterial({color: this.material.color, linewidth: 1});
        const line = new THREE.Line(geometry, material);
        this.group_marker.add(line);
    }

    //Ajout au groupe data_group
    createText() {
        const ctx = document.createElement('canvas').getContext('2d');

        ctx.canvas.width = 100;
        ctx.canvas.height = 70;

        ctx.font = 'Open Sans';
        ctx.textBaseline = 'middle';
        ctx.textAlign = 'center';

        ctx.translate(100 / 2, 70 / 2);
        ctx.scale(1, 1);
        ctx.fillStyle = 'white';
        ctx.fillText(this.labelText, 0, 0);

        return ctx.canvas;
    }


}