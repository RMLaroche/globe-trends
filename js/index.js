var csv_files = ["Bitcoin", "Coffe", "COVID", "Football", 'Illuminati', "Java"];

var group = new THREE.Group();
var data_group = new THREE.Group();

//Retourne les coordonées en fonction de la longitude et la latitude
function sphere_coordinates(lat, lng, scale) {
    var phi = (90 - lat) * Math.PI / 180;
    var theta = (180 - lng) * Math.PI / 180;
    var x = scale * Math.sin(phi) * Math.cos(theta);
    var y = scale * Math.cos(phi);
    var z = scale * Math.sin(phi) * Math.sin(theta);
    return {x, y, z};
}